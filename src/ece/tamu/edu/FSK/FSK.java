package ece.tamu.edu.FSK;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteOrder;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

@SuppressLint("NewApi")
public class FSK extends Activity {
	private static final int RECORDER_BPP = 16;
	private static final String AUDIO_RECORDER_FILE_EXT_WAV = ".wav";
	private static final String AUDIO_RECORDER_FOLDER = "AudioRecorder";
	private static final String AUDIO_RECORDER_TEMP_FILE = "record_temp.raw";
	private static final int RECORDER_SAMPLERATE = 44100;
	private static final int RECORDER_CHANNELS = AudioFormat.CHANNEL_IN_MONO;
	private static final int RECORDER_AUDIO_ENCODING = AudioFormat.ENCODING_PCM_16BIT;
	//private int[] array;
	private AudioRecord recorder = null;
	private int bufferSize = 0;
	private Thread recordingThread = null;
	private boolean isRecording = false;
	private static TextView message,process;
	private static Handler handle;
	private static String temp2="global";
	private static int maxParity = -1;
	private static int gErrorRate = -1;
	private static String results = "initialized";
	private static boolean done=true;
	//String message;
	
    @SuppressLint("NewApi")
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        setButtonHandlers();
        enableButtons(false);
        message= (TextView)findViewById(R.id.textView1);
        process=(TextView)findViewById(R.id.textView2);
       // message=(TextView) findViewById(R.)
        handle=new Handler();
        bufferSize = AudioRecord.getMinBufferSize(RECORDER_SAMPLERATE,RECORDER_CHANNELS,RECORDER_AUDIO_ENCODING);
    }

	private void setButtonHandlers() {
		((Button)findViewById(R.id.btnStart)).setOnClickListener(btnClick);
        ((Button)findViewById(R.id.btnStop)).setOnClickListener(btnClick);
	}
	
	private void enableButton(int id,boolean isEnable){
		((Button)findViewById(id)).setEnabled(isEnable);
	}
	
	private void enableButtons(boolean isRecording) {
		enableButton(R.id.btnStart,!isRecording);
		enableButton(R.id.btnStop,isRecording);
	}
		
	private void disableButtons() {
		enableButton(R.id.btnStart,false);
		enableButton(R.id.btnStop,false);
	}
	
	
	private String getFilename(){
		String filepath = Environment.getExternalStorageDirectory().getPath();
		File file = new File(filepath,AUDIO_RECORDER_FOLDER);
		
		if(!file.exists()){
			file.mkdirs();
		}
		
		return (file.getAbsolutePath() + "/" + System.currentTimeMillis() + AUDIO_RECORDER_FILE_EXT_WAV);
	}
	
	private String getTempFilename(){
		String filepath = Environment.getExternalStorageDirectory().getPath();
		File file = new File(filepath,AUDIO_RECORDER_FOLDER);
		
		if(!file.exists()){
			file.mkdirs();
		}
		
		File tempFile = new File(filepath,AUDIO_RECORDER_TEMP_FILE);
		
		if(tempFile.exists())
			tempFile.delete();
		
		return (file.getAbsolutePath() + "/" + AUDIO_RECORDER_TEMP_FILE);
	}
	
	private void startRecording(){
		recorder = new AudioRecord(MediaRecorder.AudioSource.MIC,
						RECORDER_SAMPLERATE, RECORDER_CHANNELS,RECORDER_AUDIO_ENCODING, bufferSize);
		
        handle.post(new Runnable(){
       	 public void run()
       	 {
       		 message.setText("");
       	 }
         });
		
		
		recorder.startRecording();
		
		isRecording = true;
		
		recordingThread = new Thread(new Runnable() {
			
			public void run() {
				writeAudioDataToFile();
			}
		},"AudioRecorder Thread");
		
		recordingThread.start();
	}
	
	@SuppressLint("NewApi")
	private void writeAudioDataToFile(){
		byte data[] = new byte[bufferSize];
		String filename = getTempFilename();
		ByteArrayOutputStream os = null;
		FileOutputStream os2=null;
		try {
			os=new ByteArrayOutputStream();
			os2 = new FileOutputStream(filename);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		int read = 0;
		
		if( null!=os2 && null!=os){
			while(isRecording){
				read = recorder.read(data, 0, bufferSize);
				
				if(AudioRecord.ERROR_INVALID_OPERATION != read){
					try {
						os.write(data);
						os2.write(data);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				//
			}
			
	        handle.post(new Runnable(){
	          	 public void run()
	          	 {
	     			disableButtons();
	          	 }
	            });
			
			demod(os.toByteArray());
			
	        handle.post(new Runnable(){
	          	 public void run()
	          	 {
	     			enableButtons(isRecording);
	          	 }
	            });
			
			try {
				os.close();
				os2.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	private static String temp;
	private static void settext(String s)
	{
		temp="This is an application to decode a cryptic audio message! Luke\n\nDecoding in progress"+s+"\n";
		handle.post(new Runnable(){
       	 public void run()
       	 {
       		 process.setText(temp);
       	 }
         });
	}
	
	private void demod(byte[] bytes){
		Thread processing=new Thread(new Runnable() {
			
			public void run() {
				int i=0;
				while(done){
					try{
					
					i++;
					switch(i%5){
					case 0:
						settext(".");
						break;
					case 1:
						settext("..");
						break;
					case 2:
						settext("...");
						break;
					case 3:
						settext("....");
						break;
					case 4:
						settext(".....");
						break;
					}
					Thread.sleep(1000);
					}catch(Exception e){
						e.printStackTrace();
					}
				}
				done=true;
			}
		},"Processing Thread");
		
		processing.start();
		
		double[] y=bytes2double(bytes);
	    try {
			one(y);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	    
	}
	
	private static double[] bytes2double(byte[] out2){
		short[] in=new short[(out2.length)/2];
		for(int i=0;i<(out2.length)/2;i++)
			in[i]=byteToShort(out2,2*i,ByteOrder.nativeOrder());
		double[] in2=new double[in.length];
		for(int i=0;i<in.length;i++)
			in2[i]=((double)in[i])/(1<<15);
		
		return in2;
		
	}
	
	
	  public static short byteToShort(byte[] paRawBytes, int piOffset, ByteOrder order)
	  {
	    int iRetVal = -1;
	    boolean pbBigEndian;
	    if(order==ByteOrder.BIG_ENDIAN)
	    	pbBigEndian=true;
	    else
	    	pbBigEndian=false;
	    if(paRawBytes.length < piOffset + 2)
	      return -1;
	    int iLow;
	    int iHigh;
	    if(pbBigEndian)
	    {
	      iLow  = paRawBytes[piOffset + 1];
	      iHigh = paRawBytes[piOffset + 0];
	    }
	    else
	    {
	      iLow  = paRawBytes[piOffset + 0];
	      iHigh = paRawBytes[piOffset + 1];
	    }
	    // Merge high-order and low-order byte to form a 16-bit double value.
	    iRetVal = (iHigh << 8) | (0xFF & iLow);
	    return (short)iRetVal;
	  }
	  
	  @SuppressLint("NewApi")
	private static void one(double[] y) throws InterruptedException {
		  int var=40;
	         final int symlen=128*var;
	         temp2="";
	         final double FS=44100d;
	         final int symfreq[] = {18000,19000};
	         double[][] symbols=new double [2][symlen];
	         double pi=Math.PI;
	         //System.out.println(pi);
	         //System.exit(0);
	         int prelen=128;
	         int synclen=128;
	         char[] sync={0,0,1,0,0,0,0,1,0,0,1,1,0,1,0,1,1,0,1,0,1,0,0,0,0,1,0,0,0,0,0,0,1,0,1,0,1,1,0,1,1,
	                      1,1,0,1,0,0,1,1,1,1,0,0,1,0,0,0,0,1,1,1,1,1,0,0,1,0,0,1,0,1,0,0,0,1,1,0,0,1,0,1,0,
	                      1,0,0,0,0,1,1,0,0,0,1,1,0,0,1,1,0,1,1,0,1,1,1,1,0,1,1,0,0,1,0,1,1,1,1,0,1,0,1,1,0,
	                      0,0,0,0,1};
	         double[][] z = new double[2][y.length];
	         double[][] pz= new double[2][y.length];
	         double coarsewin=pz[1].length-synclen*symlen-prelen*symlen;
	         double[] premft=new double [(int)coarsewin/symlen+1];
	         double[] premf=new double[premft.length-prelen];         
	         int prelocsym=0;

	         double[] subz = new double[synclen*2];
	         double tmp=0;
	         int t=0;
	         int k1=0;
	         char[] decmsgdata,decmsgtxt;
	         double[][] pzt;
	         char[] dec;
	         for(int i=0 ; i<2 ; i++){
	                 for(int j=0; j<symlen; j++){
		                	 double middle = symlen/2;
		                     double distance = Math.abs(j-middle);
		                     double norm = distance/middle;
		                     double flip = 1 - norm;
	                         symbols[i][j]=flip*Math.cos(2*pi*j*symfreq[i]/FS);
	                 }
	         }
	         
	         for(int i=0; i<2; i++){
	                 for(int j=0;j<y.length;j++){
	                         z[i][j]=0;
	                         for(int k=0; k<symlen && j+k<y.length; k++){
	                        	 if(j-k<0)
	                        		 continue;
	                             z[i][j]+=symbols[i][symbols[i].length-k-1]*y[j-k];
	                                 
	                         }	                         
	                         pz[i][j]=Math.pow((z[i][j]),2);
	                 }
	         }
	         symbols=null;
	         z=null;
	         y=null;
	         int attempts = 11;
	         
	         for(int h=1;h<attempts;h++)
	         {
	        	 int offset = symlen/attempts;
	        	 //pz is having it's prelen cut off each time.  I need to reset it.
		         double[][] tryAgain=new double[2][pz[0].length - h*offset+1];
		         System.arraycopy(pz[0], h*offset-1, tryAgain[0], 0, tryAgain[0].length);
		         System.arraycopy(pz[1], h*offset-1, tryAgain[1], 0, tryAgain[1].length);
		         pz=tryAgain;	         
		         
	        	 
		         for(int i=0, j=0;i<coarsewin;i+=symlen,j++){
		                 premft[j]=pz[0][i];                
		         }
		         for(int i=1;i<premft.length;i++){
		        	 premft[i]+=premft[i-1];	                 
		         }
		         
	
		         for(int i=0;i<premf.length;i++)
		         {
		        	 premf[i]=premft[i+prelen]-premft[i];
		         }
		         
	
		         double max=0;
		         for(int i=0;i<premf.length-1;i++)
		         {
		        	 if(premf[i]>max)
		        	 {
		        		 max = premf[i];
		        		 prelocsym=i;
		        	 }
		  		 }
		         
		         int prelocsam=(prelocsym+prelen-14)*symlen;
		         
		         double[][] temp=new double[2][pz[0].length - prelocsam+1];
		         System.arraycopy(pz[0], prelocsam-1, temp[0], 0, temp[0].length);
		         System.arraycopy(pz[1], prelocsam-1, temp[1], 0, temp[1].length);
		         double[][] pzcut=temp;
		         
		         int acqwin = pzcut[1].length-synclen*symlen;
		         double[] score = new double[acqwin];
		         for(int j=0;j < acqwin;j++){
		           	 for (int i = 0;i<symlen*synclen; i+=symlen){
		           		 subz[2*i/symlen]
		           		      = pzcut[0][j+i];
		           		 subz[2*i/symlen+1] = pzcut[1][j+i];
		           	 }
		           	 for (int i=0; i<2*synclen;i+=2){
		           			 score[j] += subz[sync[i/2]+i];
		           	 }
		         }
		         
		         for(int i = 0; i<acqwin; i++){
		        	 if (tmp<score[i]){
		        		 tmp = score[i];
		        		 t = i;
		        	 }
		         }
		         score=null;

		         pzt=new double[2][(pzcut[0].length-t-1)/symlen+1];
		         
		         for(int i=0;i<2;i++){
		        	 for(int j=0;j<pzt[0].length;j++){
		        		 pzt[i][j]=pzcut[i][t+(symlen*j)];}
		        	 
		         }
		         
		         dec=new char[pzt[0].length];
		         
		         for(int i=0;i<dec.length;i++){
		        	dec[i] =(char) (pzt[0][i]>pzt[1][i]? 0:1);
		         }
		         
		         decmsgdata=new char[dec.length-synclen];
		         k1=decmsgdata.length/8;
		         System.arraycopy(dec, synclen, decmsgdata, 0, decmsgdata.length);
	
		         decmsgtxt=new char[k1];
		         
		         for(int i=0;i<8*k1;i+=8){
		        	 decmsgtxt[i/8]=0;
		        	 for(int j=0;j<8;j++)
		        	 {
		        		 decmsgtxt[i/8]=(char)(decmsgtxt[i/8]<<1);
		        		 decmsgtxt[i/8]|=decmsgdata[i+j];
		        	 } 
		         }
		         decmsgtxt[decmsgtxt.length-1]=0;
		         
		         System.out.println("This is the message I got: ");
		         System.out.println(decmsgtxt);
		         String received = "";
		         
		         for(int i=0;i<decmsgtxt.length;i++){
		        	 System.out.println((int)decmsgtxt[i]);
		        	 if(decmsgtxt[i]!='\n' && i<24)
		        	 {
		        		 temp2+=decmsgtxt[i];
		        		 received+=decmsgtxt[i];
		        	 }
		        	 if(decmsgtxt[i]==0)
		        	 {
		        		 temp2+="0";
		        		 received+="0";
		        	 }
		         }
		         temp2+="\n";
		         received+="\n";
		         //////////////////////////////////////////
		         //Begin error rate code
		         //////////////////////////////////////////
		         String expected = "0@1,2^3^4l5Q6n7C8f9W";
		         //temp2 is acutally a giant string and not what I want...
		         String parity = "0123456789";
		         
		         if(received.length()>20)
		         {
		         
			         //parity score
			         int parScore = 0;
			         for(int i=0;i<20;i=i+2)
			         {
			         	if(parity.charAt(i/2)==received.charAt(i))
			         		parScore++;
			         }
			         //remove the parity characters and compare the binary strings
			         
			         String expectedStrip = "";
			         String receivedStrip = "";
			         
			         for(int i=0;i<10;i++)
			         {
			         	expectedStrip += expected.charAt(i*2+1);
			         	receivedStrip += received.charAt(i*2+1);
			         }
			         
			         //convert the ascii to binary
			         int errorCount = 0;
			         
			         for (int i=0;i<10;i++)
			         {
			         	String eBits = Integer.toBinaryString(expectedStrip.charAt(i));
			         	for (int j=0;eBits.length()<8;j++)
			         		eBits = "0"+eBits;
			         	String rBits = Integer.toBinaryString(receivedStrip.charAt(i));
			         	for (int j=0;rBits.length()<8;j++)
			         		rBits = "0"+rBits;
			         	
			         	for(int k=0;k<8;k++)
			         	{
			         		if(eBits.charAt(k)!=rBits.charAt(k))
			         			errorCount ++;
			         	}
			         }
			         if(parScore>maxParity)
			         {
			        	 maxParity = parScore;
			         	 gErrorRate = errorCount;
			         	 results = "Parity: "+maxParity+" Error: "+gErrorRate+"\n";
			         }
		         }    
		         //////////////////////////////////////////
		         //End error rate code
		         //////////////////////////////////////////
	         	 if(h==10)
	         		 temp2 += results;
		         
		         
		        done=false;
		         handle.post(new Runnable(){
		        	 public void run()
		        	 {
		        		 process.setText("Results Are:");
		        		 message.setText(temp2);
		        	 }
		          });
	         }
	         subz=null;
	         pz = null;
	         pzt = null;
	         premft=null;
	         premf=null;
}
	
	@SuppressLint("NewApi")
	private void stopRecording(){
		if(null != recorder){
			isRecording = false;
			
			recorder.stop();
			recorder.release();
			
			recorder = null;
			recordingThread = null;
		}
		
		copyWaveFile(getTempFilename(),getFilename());
		deleteTempFile();
	}

	private void deleteTempFile() {
		File file = new File(getTempFilename());
		
		file.delete();
	}
	
	private void copyWaveFile(String inFilename,String outFilename){
		FileInputStream in = null;
		FileOutputStream out = null;
		long totalAudioLen = 0;
		long totalDataLen = totalAudioLen + 36;
		long longSampleRate = RECORDER_SAMPLERATE;
		int channels = 1;
		long byteRate = RECORDER_BPP * RECORDER_SAMPLERATE * channels/8;
		
		byte[] data = new byte[bufferSize];
                
		try {
			in = new FileInputStream(inFilename);
			out = new FileOutputStream(outFilename);
			totalAudioLen = in.getChannel().size();
			totalDataLen = totalAudioLen + 36;
						
			WriteWaveFileHeader(out, totalAudioLen, totalDataLen,
					longSampleRate, channels, byteRate);
			
			while(in.read(data) != -1){
				out.write(data);
			}
			
			in.close();
			out.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void WriteWaveFileHeader(
			FileOutputStream out, long totalAudioLen,
			long totalDataLen, long longSampleRate, int channels,
			long byteRate) throws IOException {
		
		byte[] header = new byte[44];
		
		header[0] = 'R';  // RIFF/WAVE header
		header[1] = 'I';
		header[2] = 'F';
		header[3] = 'F';
		header[4] = (byte) (totalDataLen & 0xff);
		header[5] = (byte) ((totalDataLen >> 8) & 0xff);
		header[6] = (byte) ((totalDataLen >> 16) & 0xff);
		header[7] = (byte) ((totalDataLen >> 24) & 0xff);
		header[8] = 'W';
		header[9] = 'A';
		header[10] = 'V';
		header[11] = 'E';
		header[12] = 'f';  // 'fmt ' chunk
		header[13] = 'm';
		header[14] = 't';
		header[15] = ' ';
		header[16] = 16;  // 4 bytes: size of 'fmt ' chunk
		header[17] = 0;
		header[18] = 0;
		header[19] = 0;
		header[20] = 1;  // format = 1
		header[21] = 0;
		header[22] = (byte) channels;
		header[23] = 0;
		header[24] = (byte) (longSampleRate & 0xff);
		header[25] = (byte) ((longSampleRate >> 8) & 0xff);
		header[26] = (byte) ((longSampleRate >> 16) & 0xff);
		header[27] = (byte) ((longSampleRate >> 24) & 0xff);
		header[28] = (byte) (byteRate & 0xff);
		header[29] = (byte) ((byteRate >> 8) & 0xff);
		header[30] = (byte) ((byteRate >> 16) & 0xff);
		header[31] = (byte) ((byteRate >> 24) & 0xff);
		header[32] = (byte) (1 * 16 / 8);  // block align
		header[33] = 0;
		header[34] = RECORDER_BPP;  // bits per sample
		header[35] = 0;
		header[36] = 'd';
		header[37] = 'a';
		header[38] = 't';
		header[39] = 'a';
		header[40] = (byte) (totalAudioLen & 0xff);
		header[41] = (byte) ((totalAudioLen >> 8) & 0xff);
		header[42] = (byte) ((totalAudioLen >> 16) & 0xff);
		header[43] = (byte) ((totalAudioLen >> 24) & 0xff);

		out.write(header, 0, 44);
	}
	
	private View.OnClickListener btnClick = new View.OnClickListener() {
		public void onClick(View v) {
			switch(v.getId()){
				case R.id.btnStart:{
			         handle.post(new Runnable(){
			        	 public void run()
			        	 {
			        		 process.setText("This is an application to decode a cryptic audio message!\n\n\n");
			        	 }
			          });
					
					enableButtons(true);
					startRecording();
							
					break;
				}
				case R.id.btnStop:{
					
					enableButtons(false);
					stopRecording();
					
					break;
				}
			}
		}
	}; 
	

}